﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DBChangesTracker
{
    public class Binacle
    {
        private const string REQUESTER_DESC = "Requester";
        private const string AUTHORIZER_DESC = "Requester";
        private const string EXECUTOR_DESC = "Requester";

        private string _connectionString;
        private TemplateEntities _entity;
        private string _fullPath;
        public string FullPath
        {
            get { return _fullPath; }
            private set { _fullPath = value; }
        }

        public Binacle(string binacle_path)
        {
            if (!File.Exists(binacle_path))
                throw new FileNotFoundException("Binacle specified not found.");
            _fullPath = binacle_path;
            _connectionString = string.Format(
                "metadata=res://*/Template.csdl|res://*/Template.ssdl|res://*/Template.msl;provider=System.Data.SqlServerCe.4.0;provider connection string=&quot;Data Source={0};Max Database Size=1024&quot;",
                _fullPath);
            _entity = new TemplateEntities(_connectionString);
        }

        public int AddUser(string first_name, string last_name, string email_address, string company, bool requests, bool authorizes, bool executes)
        {
            if (!(requests || authorizes || executes))
                throw new ArgumentException("The user must have at least one role.");
            var new_user = new USER
            {
                USER_FIRST_NAME = first_name,
                USER_LAST_NAME = last_name,
                USER_E_MAIL_ADDRESS = email_address,
                USER_COMPANY = company
            };
            if (requests)
                new_user.PERMISSIONS.Add(
                    _entity.PERMISSIONS.First(
                        i => i.PERMISSION_DESCRIPTION == REQUESTER_DESC));
            if (authorizes)
                new_user.PERMISSIONS.Add(
                    _entity.PERMISSIONS.First(
                        i => i.PERMISSION_DESCRIPTION == AUTHORIZER_DESC));
            if (executes)
                new_user.PERMISSIONS.Add(
                    _entity.PERMISSIONS.First(
                        i => i.PERMISSION_DESCRIPTION == EXECUTOR_DESC));
            _entity.USERS.Add(new_user);
            return _entity.SaveChanges();
        }

        public int RemoveUser(USER user)
        {
            if (!(_entity.USERS.Contains(user)))
                throw new ArgumentException("User specified not found");
            _entity.USERS.Remove(user);
            return _entity.SaveChanges();
        }

        public int ChangeExistingUserPermissions(USER user, bool requests, bool authorizes, bool executes)
        {
            if (!(requests || authorizes || executes))
                throw new ArgumentException("The user must have at least one role.");
            if (!(_entity.USERS.Contains(user)))
                throw new ArgumentException("User specified not found");
            var selected_user = _entity.USERS.First(i => i.USER_ID == user.USER_ID);
            selected_user.PERMISSIONS.Clear();
            if (requests)
                selected_user.PERMISSIONS.Add(
                    _entity.PERMISSIONS.First(
                        i => i.PERMISSION_DESCRIPTION == REQUESTER_DESC));
            if (authorizes)
                selected_user.PERMISSIONS.Add(
                    _entity.PERMISSIONS.First(
                        i => i.PERMISSION_DESCRIPTION == AUTHORIZER_DESC));
            if (executes)
                selected_user.PERMISSIONS.Add(
                    _entity.PERMISSIONS.First(
                        i => i.PERMISSION_DESCRIPTION == EXECUTOR_DESC));
            return _entity.SaveChanges();
        }
    }

    public partial class TemplateEntities : DbContext
    {
        public TemplateEntities(string connection_string) : base(connection_string)
        {

        }
    }
}
